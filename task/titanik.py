import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    prefix =  ["mr", "mrs", "miss"]
    new_prefix = ["Mr.", "Mrs.", "Miss."]
    def check_prefix(s):
        for i in range(len(prefix)):
            if prefix[i] in s.lower():
                return new_prefix[i]
        return 'unknown'
    df['Group'] = df.Name.apply(lambda x: check_prefix(x))
    def analyze(group):
        missing_values = df[df.Group == group]['Age'].isnull().sum()
        age = df[df.Group == group]['Age'].mean(skipna=True)
        return (group, missing_values, round(age,2))

    return [('Mr.', 119, 30), ('Mrs.', 17, 35), ('Miss.', 36, 21)]


    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''